import React, {useState, useContext} from 'react';
import Axios from 'axios';
import { UpdateModal } from './UpdateModal';
import { employeeContext } from '../employeeContext';
import { useEffect } from 'react';

export var list = [];
export var rowCheckboxes = [];
export const UserRow = ({emp, setSuccessfulUpdation, options }) => {
    const [ employeeDetails, setEmployeeDetails ] = useContext(employeeContext);
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const [successfulDeletion, setSuccessfulDeletion] = useState(false);

    useEffect(() => {
        rowCheckboxes = document.getElementsByName("rowCheckbox");
    }, [rowCheckboxes, employeeDetails, options]);

    const addToDel = (empId) => {
        list.push(parseInt(empId));
        console.log(list)
    };

    const getElementById = () => {
        for(var i=0;i<rowCheckboxes.length;i++){
            if(rowCheckboxes[i].checked === true){
                addToDel(emp.employee_id)
            }
        }
       }
    getElementById();

    const deleteEmp = (delEmp_Id) => {
        Axios.delete('http://localhost:3001/employees', 
            {
                params: {delEmp: delEmp_Id}
            }
        ).catch(err => alert(err));
        
        /*setTimeout(() => {
            Axios.get("http://localhost:3001/employees").then((response) => {
                console.log(response.data)
                setEmployeeDetails(response.data);
            }).catch(err => alert(err));
        }, 1000);*/
        setSuccessfulDeletion(true);
    }

    const openModal = () => {
        setModalIsOpen(true);
    }

   

   

    return (
        <tr>
            <td><input id="checkbox" name="rowCheckbox" type="checkbox" onChange={() => {
                    addToDel(emp.employee_id);
            }}/></td>
            <td>{emp.employee_id}</td>
            <td>{emp.employee_name}</td>
            <td>{emp.employee_age}</td>
            <td>{emp.employee_salary}</td>
            <td>{emp.active_status}</td>
            <td><button id="editDel-btn" className="btn btn-warning btn-xs" onClick={() => openModal()}>Edit</button></td>
            <td>&nbsp;<button id="editDel-btn" className="btn btn-danger btn-xs" onClick={() => deleteEmp(emp.employee_id)}>Delete</button></td>
            <td>{successfulDeletion &&  <p id="empDel">Deleted</p>}</td> 
            {modalIsOpen && <UpdateModal emp={emp} modalIsOpen={modalIsOpen} setModalIsOpen={setModalIsOpen} setSuccessfulUpdation={setSuccessfulUpdation}/>}  
        </tr>
    )
}