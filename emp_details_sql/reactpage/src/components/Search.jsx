import React, {useEffect} from 'react';
import { useState, useContext } from 'react';
import Axios from 'axios';
import { employeeContext } from '../employeeContext';

export const Search = () => {
    const [ employeeDetails, setEmployeeDetails ] = useContext(employeeContext);
    const [ searchEmp, setSearchEmp ] = useState('');
    const [ empNotFound, setEmpNotFound ] = useState(false);

    const searchForEmployee = (searchEmp) => {
       // if(/^\d+$/.test(searchEmp)){
            Axios.get(`http://localhost:3001/employees/${searchEmp}`).then((response) => {
                console.log(response.data)

                if(response.data.length!=0 && response.data[0].employee_id !== 0){
                    setEmployeeDetails(response.data);
                }else{
                    setEmpNotFound(true);
                }
            }).catch(err => alert(err)) 

            setSearchEmp('');
       // }else{
       //     alert('Invalid Input')
        //}
        
    }

    
    return (
        <>
            <tr>
                

                <td><input id="searchText" type="text" className="form-control" placeholder="search Employee" value={searchEmp} onChange={(e) => setSearchEmp(e.target.value)}/></td>
                <td><button className="btn btn-success btn-xs" onClick={() => searchForEmployee(searchEmp)}>Search</button></td>
            </tr>
            <tr>
                <td>{empNotFound && <p style={{color:"red"}}>Employee Not Found!</p>}</td>
            </tr>
            <hr></hr>
        </>
    )
}