import React from 'react';
import { useState, useEffect } from 'react';
import Axios from 'axios';
import { UserTable } from './UserTable';
import { FieldsModal } from './FieldsModal';
import $ from 'jquery';
import { employeeContext } from '../employeeContext';
import * as jwt from 'jwt-simple';
import { CSVLink, CSVDownload } from "react-csv";
import ReactMultiSelectCheckboxes from 'react-multiselect-checkboxes';

//import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
//import { faEye } from "@fortawesome/free-solid-svg-icons";
//const eye = <FontAwesomeIcon icon={faEye} />;


var canInsert = 0;
export function Form() {
    const [passwordShown, setPasswordShown] = useState(false);
    const [employee, setEmployee] = useState({ empId: '', empName: '', empAge: '', empSalary: '', empPassword: ''})
    const [employeeDetails, setEmployeeDetails] = useState([]);
    const [successfulSubmission, setSuccessfulSubmission] = useState(false);
    const [showAddEmpForm, setShowAddEmpForm] = useState(false);
    const [fieldsModal, setFieldsModal] = useState(true);

   /*useEffect( () => {

    for( var opt in options) {
        if(options[opt]){
            if(!(fields.includes(options[opt])))
                fields.push(opt);
        }
    }
    console.log(fields)
    Axios.get('http://localhost:3001/employees', {
            params: {opt: [...new Set(fields)]
            }
       }).then((response) => {
        setEmployeeDetails(response.data);
        console.log('entered')
       })
       .catch(err => {
           alert(err);
       });
   }, [setEmployeeDetails, Axios.post, options])*/

    
    const handleChange = (event) => {
        const value = event.target.value;
        setEmployee({ 
            ...employee,
            [event.target.name]: value
        })
    }

    const submitForm = () => { 
        const employees =  employeeDetails;
        if(employees.length===0){
            canInsert = 1;
            insertEmp(); 
        }else{
            for(var i=0;i<employees.length;i++){
                if(employees[i].employee_id != employee.empId){
                    canInsert = 1;
                }else{
                    canInsert = 0;
                    break;  
                }
            }
            if(canInsert === 1) {
                insertEmp();
            }else{
                setEmployee({empId:'',empName: '', empAge:'', empSalary:''})
                alert('Employee Id Exists!!')
            }
        }
        setShowAddEmpForm(false);
    }

    const insertEmp = () => {
        var payload = employee.empPassword;
        var secret = 'fe1a1915a379f3be5394b64d14794932';
        var token = jwt.encode(payload, secret);

        if(employee.empId === '' || employee.empName === '' || employee.empAge === '' || employee.empSalary === '' || employee.empPassword === ''){
            alert('Input Fields Empty!');
        }else{
            Axios.post("http://localhost:3001/employees", {
                    employee_id: employee.empId, 
                    employee_name: employee.empName, 
                    employee_age: employee.empAge, 
                    employee_salary: employee.empSalary,
                    active_status: 1,
                    password: token
            }).catch(err => {alert(err);})
            
            /*setTimeout(() => {
                Axios.get("http://localhost:3001/api/get").then((response) => {
                    console.log(response.data)
                    setEmployeeDetails(response.data);
                }).catch(err => alert(err))
            }, 1000);*/
            setEmployee({empId:'',empName: '', empAge:'', empSalary:'', empPassword: ''})
            setSuccessfulSubmission(true);
            setTimeout(() => {$('#empAdd').fadeOut();}, 2000)
        }
        
        
    } 
     
   
    return (
        <employeeContext.Provider value={[ employeeDetails, setEmployeeDetails ]}>
            <div className="form">
                <h1>Employee Details</h1>
                {/* {fieldsModal && <FieldsModal fieldsModal={fieldsModal} setFieldsModal={setFieldsModal}/>} */}
                <UserTable />
                
                <button onClick={() => setShowAddEmpForm(true)}>Add Employee</button>

                <CSVLink data={employeeDetails}>Download</CSVLink>

                {successfulSubmission &&  <h2 id="empAdd">Employee Added!</h2>}
                {showAddEmpForm && 
                <div>
                    <h2>Add Employee!</h2>
                    <label>Employee Id</label>
                    <input className="form-control" type="text" name="empId" value={employee.empId} onChange={handleChange}/>
                    <label>Employee Name</label>
                    <input className="form-control" type="text" name="empName" value={employee.empName} onChange={handleChange}/>
                    <label>Age</label>
                    <input className="form-control" type="text" name="empAge" value={employee.empAge} onChange={handleChange}/>
                    <label>Salary</label>
                    <input className="form-control" type="text" name="empSalary" value={employee.empSalary} onChange={handleChange}/>
                    <label>Set Password</label>
                    <input className="form-control" type={passwordShown ? "text" : "password"} name="empPassword" value={employee.empPassword} onChange={handleChange}/>
                    <button id="submit-btn" className="btn btn-primary" onClick={submitForm}>Submit</button>&nbsp;
                    <button id="submit-btn" className="btn btn-danger" onClick={() => setShowAddEmpForm(false)}>Cancel</button>
                </div> }   
                
                
                <br></br>
                
                <div id="root">
            
                </div>
            </div>
        </employeeContext.Provider>
        
    )
}