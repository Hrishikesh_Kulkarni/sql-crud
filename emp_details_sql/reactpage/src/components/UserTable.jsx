import React, {useState, useContext} from 'react';
import { UserRow } from './UserRow';
import { Search } from './Search';
import { EmpPagination }  from './EmpPagination';
import { employeeContext } from '../employeeContext';
import Axios from 'axios';
import { list, rowCheckboxes } from './UserRow'; 
import { useEffect } from 'react';
import { FieldsModal } from './FieldsModal';

export const UserTable = () => {
    const [employeeDetails, setEmployeeDetails] = useContext(employeeContext);
    const [successfulUpdation, setSuccessfulUpdation] = useState(false);
    const [ options, setOptions ] = useState({employee_id: true, employee_name: true, employee_age: true, employee_salary: false, active_status: false})
    const [ itemsPerPage, setItemsPerPage ] = useState(5);
    const [ checked, setChecked ] = useState(false);
    const [ rowsCheckbox, setRowsCheckbox ] = useState(true);
    const [ displayFieldsModal, setDisplayFieldsModal] = useState(false);
    const [ empIdSortFlag, setEmpIdSortFlag ] = useState(false);
    const [ empNameSortFlag, setEmpNameSortFlag ] = useState(false);
    const empData = employeeDetails;

   /* const onPaginationChange = (start, end) => {
        setPagination({start: start, end: end});
    }*/

    useEffect(() => {
        setOptions(options);
        console.log(itemsPerPage)
    },[options, setOptions])
    const handleSort = (type) => {
        sortRecords([...empData], type);
    }

    const sortRecords = (empData, type) => {
        /*Axios.get(`http://localhost:3001/api/sort/${type}/${pagination.start}/${pagination.end}`).then((response) => {
            console.log(response.data)
            setEmployeeDetails(response.data);
        })*/
        let sortedEmp = [];
        if(!empNameSortFlag && type === 'employee_name'){
            sortedEmp = empData.sort((a, b) => {
                return a.employee_name > b.employee_name ? 0 : -1;
            });
            setEmpNameSortFlag(true);
        }else if(empNameSortFlag && type === 'employee_name') {
            sortedEmp = empData.sort((a, b) => {
                return a.employee_name < b.employee_name ? 0 : -1;
            });
            setEmpNameSortFlag(false);
        }else if(!empIdSortFlag && type === 'employee_id'){

                sortedEmp = empData.sort((a, b) => {
                    return a.employee_id < b.employee_id ? 0 : -1;
                });
                setEmpIdSortFlag(true);
        }else if(empIdSortFlag && type === 'employee_id'){
                sortedEmp = empData.sort((a, b) => {
                    return a.employee_id > b.employee_id ? 0 : -1;
                });
                setEmpIdSortFlag(false);
            }
  
        console.log(sortedEmp)
        setEmployeeDetails(sortedEmp);
    }

    const deleteAll = (list) => {
        console.log(list)
        Axios.delete('http://localhost:3001/employees', 
            {
                params: {delEmp: [...new Set(list)]}
            }
        ).catch(err => alert(err));
        
        /*setTimeout(() => {
            Axios.get("http://localhost:3001/employees").then((response) => {
                console.log(response.data)
                setEmployeeDetails(response.data);
            }).catch(err => alert(err));
        }, 1000);*/
        //setSuccessfulDeletion(true);
    }

    const handleClick = (event) => {
        
        const value = Boolean(event.target.value);
        //console.log(value+" "+fieldsCheckbox)
        //console.log(options.employee_name+" name")
        //console.log(typeof event.target.name)

        if(options[event.target.name] === true){
            setOptions({
                ...options,
                [event.target.name] : false
            })
        }else{
            setOptions({
                ...options,
                [event.target.name] : true
            })
        }
    } 

    const toggle = (source) => {
        var checkboxes = rowCheckboxes;
        
        if(rowsCheckbox){
            for(var i=0; i<checkboxes.length;i++){
                checkboxes[i].checked = !checked;
                console.log(checkboxes[i]+ " " + checkboxes[i].checked)
            }
            setRowsCheckbox(false);
        }else{
            
            for(var i=0; i<checkboxes.length;i++){
                checkboxes[i].checked = checked;
            }
            setRowsCheckbox(true);
        }
    }
    const handleSelect = (e) => {
        var value = e.currentTarget.value;
        console.log(value);
        setItemsPerPage(parseInt(value));
    }

    const displayMoreFields = () => {
        setDisplayFieldsModal(true);
    }
    return(
        <>

            <div className="empTable">
                

                <Search/>

                <label id="noOfRows">Number of rows</label>
                <select id="pagesize" value={itemsPerPage} onChange={(e) => handleSelect(e)}>
                    <option value="5">Pg-sz-5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                </select>
                <button onClick={() => displayMoreFields()}>More Fields?</button>
                

                {successfulUpdation &&  <h2 id="empUpdate">Employee Updated!</h2>}
                <hr></hr>
                <tr>
                    <th><input id="checkbox" type="checkbox" onClick={()=> toggle(this)}/>&nbsp;&nbsp;check all</th>
                    <th id="toSortEmpId" name="employee_id" onClick={() => handleSort('employee_id')}>&nbsp;&nbsp;Emp ID |&nbsp;</th>
                    <div id="hide1">sort desc</div>
                    <th id="toSortEmpName" onClick={() => handleSort('employee_name')}>&nbsp;&nbsp;<input id="checkbox" name="employee_name" checked="true" type="checkbox" value={!(options.employee_name)} onClick={(e) => handleClick(e)}/>Employee Name |&nbsp;</th>
                    <div id="hide2">sort asc</div>
                    <th>&nbsp;&nbsp;<input id="checkbox" name="employee_age"  checked="true" type="checkbox" value={!(options.employee_age)} onClick={(e) => handleClick(e)}/>Emp Age |&nbsp;</th>
                    {displayFieldsModal && <FieldsModal displayFieldsModal={displayFieldsModal} setDisplayFieldsModal={setDisplayFieldsModal} options={options} setOptions={setOptions}/>}

                    {options.employee_salary && <th>&nbsp;&nbsp;<input id="checkbox" name="employee_salary" checked={options.employee_salary} type="checkbox" value={!(options.employee_salary)} onClick={(e) => handleClick(e)}/>Emp Salary |&nbsp;</th>}
                    {options.active_status && <th>&nbsp;&nbsp;<input id="checkbox" name="active_status" checked={options.active_status} type="checkbox" value={!(options.active_status)} onClick={(e) => handleClick(e)}/>Emp Active?&nbsp;</th>}

                </tr>
                {empData.map((emp, index) => <UserRow key={index+1} emp={emp} setSuccessfulUpdation={setSuccessfulUpdation} options={options}/>)}
                <td><button onClick={() => deleteAll(list)} className="btn btn-danger btn-xs">Delete Checked</button></td>
            </div>
            <div id="marginPagination">
                <EmpPagination itemsPerPage={itemsPerPage}  totalRecords={employeeDetails.length} options={options}/>
            </div>
        </>
    )
}