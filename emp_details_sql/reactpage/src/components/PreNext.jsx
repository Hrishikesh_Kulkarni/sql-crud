import React from 'react';
import * as cp from './CurrentPage';

export const PrevNext = ({users, movFor, movBack, display}) => {
    return (
        <div>
            <button onClick={() => {movBack()}}>Prev</button>
            <button onClick={() => {movFor()}}>Next</button>
        </div>  
    )
    
}