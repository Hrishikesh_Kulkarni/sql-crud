import React, { useState, useEffect } from 'react';
import './App.css';
import * as users from './data.json';
import { Form } from './components/Form';
import { UserTable } from './components/UserTable';
import { PrevNext } from './components/PreNext';
import { CurrentPage } from './components/CurrentPage';

var data = users.data;
var count = 0;

function App() {  
    /*const [usersCollection, setUsersCollection] = useState(users.data.slice(0, 10));
    var [pagination, setPagination] = useState({pageNum: 1, currentStart: 0, currentEnd: 10});
    const rowsPerPage = 10;
    const totalPages = Math.ceil(data.length/rowsPerPage);
    
    function sortValues(userSet) {
        count = count + 1;
        if((count % 2) !== 0){
          console.log('sort')
          const sortedUsersCollection = userSet.sort((a, b) => {
            return a.employee_name > b.employee_name ? 0 : -1;
          });
          setUsersCollection(sortedUsersCollection);
        }else{
          const sortedUsersCollection = userSet.sort((a, b) => {
            return a.employee_name < b.employee_name ? 0 : -1;
          });
          setUsersCollection(sortedUsersCollection);
        }
        
        
      }
      
      function handleSort(){
        sortValues([...usersCollection]);
      }
      
      function nextPage(pageNum, currentStart, currentEnd){

        pagination.pageNum = pageNum + 1;
        if(pagination.pageNum>totalPages){
          pagination.pageNum = pageNum;
          pagination.currentStart = currentStart;
          pagination.currentEnd = currentEnd;
          setPagination(pagination);
        }else{
          pagination.currentStart = currentStart + rowsPerPage;
          pagination.currentEnd = currentEnd + rowsPerPage;
          setPagination(pagination);
        }
        
        const updateUsersCollection = data.slice(pagination.currentStart, pagination.currentEnd);
        setUsersCollection(updateUsersCollection)
     }
        function movFor(){
            nextPage(pagination.pageNum, pagination.currentStart, pagination.currentEnd)
        }
       
    
      function prevPage(pageNum, currentStart, currentEnd){
        pagination.pageNum = pageNum - 1;
        if(pagination.pageNum<=0){
          pagination.pageNum = pageNum;
          pagination.currentStart = currentStart;
          pagination.currentEnd = currentEnd;
          setPagination(pagination);
          
        }else{
          pagination.currentStart = currentStart - rowsPerPage;
          pagination.currentEnd = currentEnd - rowsPerPage;
          setPagination(pagination);
        }

        const updateUsersCollection = data.slice(pagination.currentStart, pagination.currentEnd);
        setUsersCollection(updateUsersCollection)
        
        
        
      }

      function movBack(){
        prevPage(pagination.pageNum, pagination.currentStart, pagination.currentEnd)
      }
      function display(){
        const updateUsersCollection = data.slice(0, 10);
        setUsersCollection(updateUsersCollection);
        return <CurrentPage />
      }*/

  return (
    // <div className="App">
    //   <header className="App-header">
    //     <p>
    //       <h2>Employee Details Portal</h2>
    //     </p>
    //     <UserTable users={usersCollection} handleSort={handleSort} />
    //     <div id="root">
    //         <PrevNext users={data} movFor={movFor} movBack={movBack} display={display}/>
    //     </div>
    //   </header>
    // </div>
    <div className="App">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossOrigin="anonymous"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossOrigin="anonymous"/>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossOrigin="anonymous"></script>
      
      <Form />
    </div>
  );

}

export default App;
