import React, {useState, useEffect} from 'react';
import $ from 'jquery';
import * as jwt from 'jwt-simple';
import Axios from 'axios';
import { employeeContext } from '../employeeContext';

var canInsert = 0;

export const AddEmp = () => {
    const [employee, setEmployee] = useState({ empId: '', empName: '', empAge: '', empSalary: '', empPassword: ''})
    const [employeeDetails, setEmployeeDetails] = useState([]);
    const [successfulSubmission, setSuccessfulSubmission] = useState(false);

    useEffect(() => {
        Axios.get('http://localhost:3001/emp').then((response) => {
         setEmployeeDetails(response.data);
         console.log('entered')
        })
        .catch(err => {
            alert(err);
        });
    }, [setEmployeeDetails, Axios.post])
    
    const handleChange = (event) => {
        const value = event.target.value;
        setEmployee({ 
            ...employee,
            [event.target.name]: value
        })
    }

    const submitForm = () => { 
        const employees =  employeeDetails;
        if(employees.length===0){
            canInsert = 1;
            insertEmp(); 
        }else{
            for(var i=0;i<employees.length;i++){
                if(employees[i].employee_id != employee.empId){
                    canInsert = 1;
                }else{
                    canInsert = 0;
                    break;  
                }
            }
            if(canInsert === 1) {
                insertEmp();
            }else{
                setEmployee({empId:'',empName: '', empAge:'', empSalary:''})
                alert('Employee Id Exists!!')
            }
        }
    }

    const insertEmp = () => {
        var payload = employee.empPassword;
        var secret = 'fe1a1915a379f3be5394b64d14794932';
        var token = jwt.encode(payload, secret);

        if(employee.empId === '' || employee.empName === '' || employee.empAge === '' || employee.empSalary === '' || employee.empPassword === ''){
            alert('Input Fields Empty!');
        }else{
            Axios.post("http://localhost:3001/emp", {
                    employee_id: employee.empId, 
                    employee_name: employee.empName, 
                    employee_age: employee.empAge, 
                    employee_salary: employee.empSalary,
                    active_status: 1,
                    password: token
            }).catch(err => {alert(err);})
            
            /*setTimeout(() => {
                Axios.get("http://localhost:3001/api/get").then((response) => {
                    console.log(response.data)
                    setEmployeeDetails(response.data);
                }).catch(err => alert(err))
            }, 1000);*/
            setEmployee({empId:'',empName: '', empAge:'', empSalary:'', empPassword: ''})
            setSuccessfulSubmission(true);
            setTimeout(() => {$('#empAdd').fadeOut();}, 2000)
        }
        
        
    } 
    return  (
        <employeeContext.Provider value={[ employeeDetails, setEmployeeDetails ]}>

        <div className="addEmployee">
                <h2>Add Employee!</h2>
                {successfulSubmission &&  <h2 id="empAdd">Employee Added!</h2>}
                <label>Employee Id</label>
                <input className="form-control" type="text" name="empId" value={employee.empId} onChange={handleChange}/>
                <label>Employee Name</label>
                <input className="form-control" type="text" name="empName" value={employee.empName} onChange={handleChange}/>
                <label>Age</label>
                <input className="form-control" type="text" name="empAge" value={employee.empAge} onChange={handleChange}/>
                <label>Salary</label>
                <input className="form-control" type="text" name="empSalary" value={employee.empSalary} onChange={handleChange}/>
                <label>Set Password</label>
                {/* <i onClick={togglePasswordVisiblity}>{eye}</i> */}
                <button id="submit-btn" className="btn btn-primary" onClick={submitForm}>Submit</button>
        </div>
        </employeeContext.Provider>
      
    )
}