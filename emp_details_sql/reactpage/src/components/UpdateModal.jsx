import React, {useState, useContext} from 'react';
import Modal from 'react-modal';
import $ from 'jquery';
import Axios from 'axios';
import { employeeContext } from '../employeeContext';
import { AdminModal } from './AdminModal';

Modal.setAppElement('#root');

export const UpdateModal = ({emp, modalIsOpen, setModalIsOpen, setSuccessfulUpdation}) => {
    const [ employeeDetails, setEmployeeDetails ] = useContext(employeeContext);
    const [newEmpName, setNewEmpName] = useState('');
    const [newEmpAge, setNewEmpAge] = useState('');
    var [adminModalOpen, setAdminModalOpen] = useState(false);
    const updateEnabled = newEmpName.length > 0 && newEmpAge.length > 0;

    const customStyles = {
        content : {
          top                   : '50%',
          left                  : '50%',
          right                 : 'auto',
          bottom                : 'auto',
          marginRight           : '-50%',
          transform             : 'translate(-50%, -50%)',
          height                : '50%',
          width                 : '40%'
        }
    };


    const updateEmp = (updateEmp_Id) => {
        Axios.put("http://localhost:3001/employees", 
        {
            employee_id: updateEmp_Id,
            newEmployee_id: updateEmp_Id,
            employee_name : newEmpName,
            employee_age: newEmpAge,
            employee_salary: emp.employee_salary
        }).catch(err => alert(err))  

        /*setTimeout(() => {
            Axios.get("http://localhost:3001/api/get").then((response) => {
                console.log(response.data)
                setEmployeeDetails(response.data);
            }).catch(err => alert(err))
        }, 1000);*/
        setModalIsOpen(false);
        setSuccessfulUpdation(true);
        setTimeout(() => {$('#empUpdate').fadeOut();}, 2000)
    }

    const openAdminModal = () => {
        adminModalOpen = true;
        setAdminModalOpen(adminModalOpen);
    }

    return (
        <>
        <Modal 
            isOpen={modalIsOpen} 
            onRequestClose={() => setModalIsOpen(false)}
            style={customStyles}
        >
            <h2>Update Employee Details</h2>
            <div className="updateForm">
                <label>Employee Name</label>
                <input type="text" class="form-control" onChange={(e) => setNewEmpName(e.target.value)}/>
                <label>Employee Age</label>
                <input type="text" class="form-control" onChange={(e) => setNewEmpAge(e.target.value)}/>
            </div>
            <br></br>
            <button class="btn btn-primary" onClick={() => {updateEmp(emp.employee_id)}} disabled={!updateEnabled}>Update</button>&nbsp;
            <button class="btn btn-danger" onClick={() => setModalIsOpen(false)}>Close</button>
            &nbsp;&nbsp;<button class="btn btn-outline-primary btn-sm" onClick={() => openAdminModal()}>Advanced</button>
            {adminModalOpen && <AdminModal emp={emp} adminModalOpen={adminModalOpen} setAdminModalOpen={setAdminModalOpen}/>}

        </Modal>
        
        </>
        
    )
    
}
