const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const mysql = require('mysql');


// create connection with mysql database
const db = mysql.createConnection({
    host: 'localhost',
    user: 'Hrishikesh',
    password: '123',
    database: 'employee_details'
});

// tell express to make cross-origin requests
app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({extended: true}));

// check for successful mysql server connection
db.connect((err) => {
    if(err) throw err;
    console.log('Connected to Mysql Server');
});

var sqlSelect = '';
/*app.get('/employees', (req, res) => {
    sqlSelect = "SELECT";
    const len = req.query.opt.length;

    for(field in req.query.opt){
        if(len == 1){
            sqlSelect += " " + req.query.opt[field] + " ";
        }else if(field == (len-1)) {
            sqlSelect += " " + req.query.opt[field] + " ";
        }else{
            sqlSelect += " " + req.query.opt[field] + ",";
        }
    }
    sqlSelect += " from employee"
    console.log(sqlSelect)
            db.query(sqlSelect, (err, result) => {
                if(err) res.send(err);
                else {
                    console.log(result);
                    //result = paginatedResults(result);
                    res.send(result);
                }
            });
});*/

app.post('/employees', (req, res) => {
    console.log('post entered')
    const {employee_id, employee_name, employee_age, employee_salary, active_status, password} = req.body;
    const sqlInsert = "INSERT INTO employee (employee_id, employee_name, employee_age, employee_salary, active_status, password) VALUES (?,?,?,?,?,?)" 
    db.query(sqlInsert, [employee_id, employee_name, employee_age, employee_salary, active_status, password], (err, result) => {
        if(err){
            res.send(err);
        }
    });
});

app.delete('/employees', (req, res) => {
    console.log('del entered')
    //const len = req.query.delEmp.length;
    console.log(req.query.delEmp);
    const getActiveStatus = "SELECT active_status FROM employee";

    db.query(getActiveStatus, (err, result) => {
        if(err) res.send(err);
        else{
            const setActiveStatus = "UPDATE employee SET active_status = ? WHERE employee_id = ?"
            for(var i in req.query.delEmp){
                console.log(i);
                db.query(setActiveStatus, [0, parseInt(req.query.delEmp[i])], (err, result) => {
                    if(err) res.send(err);
                });
            }
                    
        }
    });
});

app.put('/employees', (req, res) => {
    console.log('put entered')
    const {newEmployee_id, employee_id, employee_name, employee_age, employee_salary} = req.body;
    const sqlUpdate = "UPDATE employee SET employee_id = ?, employee_name = ?, employee_age = ?, employee_salary = ? WHERE employee_id = ?"
    db.query(sqlUpdate, [newEmployee_id, employee_name, employee_age, employee_salary, employee_id], (err, result) => {
        if(err) console.log(err);
        else{
            //console.log(result);
            res.send(result);
        }
    });
});

app.get('/employees/:searchEmp', (req, res) => {
    const searchEmp = req.params.searchEmp;
    console.log(searchEmp)
    const sqlSearch = 'SELECT * from employee WHERE NOT employee_id = 0 AND employee_id = ?  OR employee_name = ? OR employee_age = ? OR employee_salary = ?'

    db.query(sqlSearch , [searchEmp,searchEmp,searchEmp,searchEmp], (err, result) => {
        if(err) res.send(err);
        else {
            console.log(result)
            res.send(result);
        }
    });
});

app.get('/employees', (req, res) => {
    console.log('ajsdga asshhdgsahdb assjddggasj')

    const pagenum = parseInt(req.query.counter);
    const pagesize = parseInt(req.query.itemsPerPage);
    console.log(pagenum+" "+pagesize)
    console.log(req.query.option)

    //const startIndex = (pagenum - 1) * pagesize;
    //const endIndex = pagenum * pagesize;
    var totalEmp = 0;
    const results = {}
    const offset = (pagenum - 1) * pagesize;

    const sqlAllEmp = 'SELECT COUNT(*) as totalEmp from employee';
    db.query(sqlAllEmp, (err, result) => {
        totalEmp = result[0].totalEmp;
        const totalPages = Math.ceil(totalEmp / pagesize);
        
        console.log(pagenum+" "+'Pagenum')
        
            if(pagenum<=0){
                results.pagenum = {pagenum: 1}
                res.send(results);
                console.log('entered < 0')
            }else if(pagenum>totalPages){
                results.pagenum = {pagenum: pagenum - 1}
                res.send(results);
                console.log('entered > 0')
            }
        else{
            sqlSelect = "SELECT ";
            const len = req.query.option.length;
            console.log(req.query.option)
            for(field in req.query.option){
                console.log(field)
                if(len == 1){
                    sqlSelect += " " + req.query.option[field] + " ";
                }else if(field == (len-1)) {
                    sqlSelect += " " + req.query.option[field] + " ";
                }else{
                    sqlSelect += " " + req.query.option[field] + ",";
                }
            }
            sqlSelect += " from employee LIMIT ? OFFSET ?"
            console.log(sqlSelect)
            //const sqlSelect = `SELECT * from employee LIMIT ${pagesize} OFFSET ${offset}`
            db.query(sqlSelect, [pagesize, offset], (err, result) => {
                if(err) console.log(err);
                else {console.log(result)};
                //results.results = result.slice(startIndex, endIndex);
                results.results = result;
                results.pagenum = {pagenum: pagenum}
                results.totalPages = {tp: totalPages}
                res.send(results);
            });
        }
        
        
    });
    
})

/*function _ProcessRequest(req, res, next){
    console.log(req.method)
    if(req.method !== 'GET' && req.method !== 'POST' && req.method !== 'PUT' && req.method !== 'DELETE'){
        return next();
    }else{
        if(req.method === 'GET'){
            console.log('GET-enete')

            const sqlSelect = "SELECT employee_id, employee_name, employee_age, employee_salary, active_status FROM employee WHERE NOT employee_name='admin'" 
            db.query(sqlSelect, (err, result) => {
                if(err) res.send(err);
                else {
                    //console.log(result);
                    //result = paginatedResults(result);
                    res.send(result);
                }
            });
        }else if(req.method === 'POST'){
            console.log('POST-enete')

            const {employee_id, employee_name, employee_age, employee_salary, active_status, password} = req.body;
            const sqlInsert = "INSERT INTO employee (employee_id, employee_name, employee_age, employee_salary, active_status, password) VALUES (?,?,?,?,?,?)" 
            db.query(sqlInsert, [employee_id, employee_name, employee_age, employee_salary, active_status, password], (err, result) => {
                if(err){
                    res.send(err);
                }
            });
        }else if(req.method === 'PUT'){
            console.log('PUT-enete')
            const {newEmployee_id, employee_id, employee_name, employee_age, employee_salary} = req.body;
            console.log(req.body)
            const sqlUpdate = "UPDATE employee SET employee_id = ?, employee_name = ?, employee_age = ?, employee_salary = ? WHERE employee_id = ?"
            db.query(sqlUpdate, [newEmployee_id, employee_name, employee_age, employee_salary, employee_id], (err, result) => {
                if(err) console.log(err);
                else{
                    console.log(result);
                    res.send(result);
                }
            });
        }else if(req.method === 'DELETE'){
            var array = req.query.delEmp.slice(' ');
            var intArr = [], j=0;
            for(var i=0;i<array.length;i++){
                console.log('array[i]'+array[i])
                if(array[i]!== ' '){
                    intArr[j++] = parseInt(array[i]);
                }
            }
    
            const getActiveStatus = "SELECT active_status FROM employee";
            db.query(getActiveStatus, (err, result) => {
                if(err) res.send(err);
                else{
                    const setActiveStatus = "UPDATE employee SET active_status = ? WHERE employee_id = ?"
                    for(var i=0;i<intArr.length;i++){
                        db.query(setActiveStatus, [0, intArr[i]], (err, result) => {
                            if(err) res.send(err);
                        });
                    }
                    
                }
            });
        }
    }
}*/

// get request to fetch employee details from db
/*app.get("/api/get", (req, res) => {
    const sqlSelect = "SELECT employee_id, employee_name, employee_age, employee_salary, active_status FROM employee WHERE NOT employee_name='admin'" 
    db.query(sqlSelect, (err, result) => {
        if(err) res.send(err);
        else res.send(result);
    });
});*/

// post request to insert new employee record
/*app.post("/api/insert", (req, res) => {
    const empId = req.body.employee_id;
    const empName = req.body.employee_name
    const empAge= req.body.employee_age;
    const empSalary = req.body.employee_salary;
    const activeStatus = req.body.active_status;
    const password = req.body.password;

    
        const sqlInsert = "INSERT INTO employee (employee_id, employee_name, employee_age, employee_salary, active_status, password) VALUES (?,?,?,?,?,?)" 
        db.query(sqlInsert, [empId, empName, empAge, empSalary, activeStatus, password], (err, result) => {
            if(err){
                res.send(err);
            }
        });
     
});*/

// delete request to delete emloyee record
/*app.delete("/api/delete/:delEmp_Id", (req, res) => {
    const getActiveStatus = "SELECT active_status FROM employee";
    db.query(getActiveStatus, (err, result) => {
        if(err) res.send(err);
        else{
            const setActiveStatus = "UPDATE employee SET active_status = ? WHERE employee_id = ?"
            db.query(setActiveStatus, [0, req.params.delEmp_Id], (err, result) => {
                if(err) res.send(err);
            })
        }
    })
    /*const sqlDelete = `DELETE FROM employee WHERE employee_id = ${req.params.delEmp_Id}` 
    db.query(sqlDelete, (err, result) => {
        if(err) console.log(err);
    })
});*/

// put request to update employee details
/*app.put("/api/update", (req, res) => {
    const toUpdateEmpId = req.body.employee_id;
    const updatedEmpId = req.body.newEmployee_id;
    const newEmpName = req.body.employee_name;
    const newEmpAge= req.body.employee_age;
    const newEmpSalary = req.body.employee_salary;


    const sqlUpdate = "UPDATE employee SET employee_id = ?, employee_name = ?, employee_age = ?, employee_salary = ? WHERE employee_id = ?"
    db.query(sqlUpdate, [updatedEmpId, newEmpName, newEmpAge, newEmpSalary, toUpdateEmpId], (err, result) => {
        if(err) res.send(err);
        else{
            console.log(result);
            res.send(result);
        }
    });
});*/

// search api
/*app.get("/api/search/:searchEmp", (req, res) => {
    const searchEmp = req.params.searchEmp;
    console.log(searchEmp)
    const sqlSearch = `SELECT * from employee WHERE employee_id = ${req.params.searchEmp}`

    db.query(sqlSearch , (err, result) => {
        if(err) res.send(err);
        else {
            console.log(result)
            res.send(result);
        }
    });
});*/

app.get("/api/auth/:username&:password", (req, res) => {
    //const password = req.params.password;
   //const username = req.params.username;

    console.log(req.params.password)
    const sqlAuth = `SELECT * from employee WHERE employee_name = '${req.params.username}' and password = '${req.params.password}'`
    db.query(sqlAuth, (err, result) => {
        if(err) res.send(err);
        else{
            console.log(result);
            res.send(result);
        } 
    })
})
// sort api
// app.get("/api/sort/:type/:start/:end", (req, res) => {
//     const start = Number(req.params.start);
//     const end = (Number(req.params.end) + 1);

//     if(req.params.type == 'employee_id'){
//         var sqlSort = `SELECT * FROM employee WHERE employee_id > ${start} AND employee_id < ${end} ORDER BY ${req.params.type} DESC`
//     }else{
//         var sqlSort = `SELECT * FROM employee WHERE employee_id > ${start} AND employee_id < ${end} ORDER BY ${req.params.type}`
//     }

//     db.query(sqlSort, (err, result) => {
//         if(err) console.log(err);
//         else res.send(result);
//     });
// });

function paginatedResults(model) {
    console.log(model)
    return ((req, res, next) => {
        const page = parseInt(req.query.page);
        const limit = parseInt(req.query.limit);

        const startIndex = (page - 1) * limit;
        const endIndex = page * limit;

        const results = {}

        if(endIndex < model.length) {
            results.next = {
                page: page + 1,
                limit: limit
            }
        }
        if(startIndex > 0) {
            results.prev = {
                page: page - 1,
                limit: limit
            }
        }

        results.results = users.slice(startIndex, endIndex);
        res.json(results);
    });
}

// server.js runs on port 3001
app.listen(3001, function(err){
    if(err){
        console.log(err);
    }else{
        console.log("Server running on port 3001");
    }
})