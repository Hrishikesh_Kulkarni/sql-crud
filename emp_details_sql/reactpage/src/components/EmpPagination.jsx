import React, { Component, useContext } from "react";
import { employeeContext } from "../employeeContext";
import { useState } from "react";
import { useEffect } from "react";
import Axios from "axios";

var fields = [];

export const EmpPagination = ({
  itemsPerPage,
  onPaginationChange,
  totalRecords,
  options,
}) => {
  var [counter, setCounter] = useState(1);
  const totalPages = Math.ceil(totalRecords / itemsPerPage);
  const [employeeDetails, setEmployeeDetails] = useContext(employeeContext);
  const [nextEnabled, setNextEnabled] = useState(true);
  const [prevEnabled, setPrevEnabled] = useState(false);
  useEffect(() => {
    /*const value = itemsPerPage * counter;
    onPaginationChange((value-itemsPerPage), (value));*/
    if(counter!=1 && itemsPerPage > 10){
      console.log('entered')
      setCounter(1);
    }
  }, [counter, itemsPerPage]);

  
  const changeFieldsList = () => {
    //console.log(options)

    for (var opt in options) {
      var index = fields.indexOf(opt);
      if (options[opt]) {
        if (!fields.includes(opt)) {
          fields.push(opt);
        } 
      } else if(!options[opt]) {
        if(fields.includes(opt)){
          fields = fields.slice(0, index).concat(fields.slice(index+1, fields.length))
        }
      }
    }
    
    //console.log(fields);

  }

  changeFieldsList();

  useEffect(() => {

    //changeFieldsList();
    
    Axios.get("http://localhost:3001/employees", {
      params: {
        counter: counter,
        itemsPerPage: itemsPerPage,
        option: fields,
      },
    }).then((response) => {
      setCounter(response.data.pagenum.pagenum);

      setEmployeeDetails(response.data.results);
    });
  }, [options, itemsPerPage]);

  const changePage = (type) => {
    console.log("prev" + fields);
    if (type === "prev") {
      console.log("Counter" + counter);
      counter = counter - 1;
      Axios.get("http://localhost:3001/employees", {
        params: {
          counter: counter,
          itemsPerPage: itemsPerPage,
          option: fields,
        },
      }).then((response) => {
        console.log("prev" + " " + response.data.pagenum.pagenum);
        if (counter === 1) {
          setPrevEnabled(false);
        }
        setCounter(response.data.pagenum.pagenum);
        console.log(response.data);

        //setCounter(response.data.prev.pagenum)
        if (response.data.results) {
          setEmployeeDetails(response.data.results);
        }
      });
    } else if (type === "next") {
      console.log("next" + fields);

      setPrevEnabled(true);
      counter = counter + 1;
      console.log("next count" + " " + counter);

      Axios.get("http://localhost:3001/employees", {
        params: {
          counter: counter,
          itemsPerPage: itemsPerPage,
          option: fields,
        },
      }).then((response) => {
        console.log("next" + " " + response.data);
        if (counter === response.data.totalPages.tp) {
          setNextEnabled(false);
        }
        setCounter(response.data.pagenum.pagenum);

        console.log(response.data);

        //setCounter(response.data.next.pagenum);
        if (response.data.results) {
          setEmployeeDetails(response.data.results);
        }
      });
    }
  };
  return (
    <div>
      <button
        id="paginationButton"
        className="btn btn-xs"
        onClick={() => changePage("prev")}
        disabled={!prevEnabled}
      >
        Previous
      </button>
      {new Array(3).fill(" ").map((ele, index) => (
        <button
          id="paginationButton"
          key={index + 1}
          className="btn btn-secondary btn-xs"
          onClick={() => {
            counter = index + 1;
            setPrevEnabled(true);
            Axios.get("http://localhost:3001/employees", {
              params: {
                counter: counter,
                itemsPerPage: itemsPerPage,
                option: fields,
              },
            }).then((response) => {
              if (counter === 1) {
                setPrevEnabled(false);
              } else {
                setPrevEnabled(true);
              }
              if (counter === response.data.totalPages.tp) {
                setNextEnabled(false);
              } else {
                setNextEnabled(true);
              }
              setCounter(response.data.pagenum.pagenum);
              setEmployeeDetails(response.data.results);
            });
          }}
          className={`page-item ${index + 1 === counter ? "btn active" : null}`}
        >
          {index + 1}
        </button>
      ))}
      <button
        id="paginationButton"
        className="btn btn-secondary btn-xs"
        onClick={() => changePage("next")}
        disabled={!nextEnabled}
      >
        &nbsp;Next&nbsp;&nbsp;
      </button>
    </div>
  );
};
