import React, {useState} from 'react';
import Modal from 'react-modal';
import { useContext } from 'react';
import { employeeContext } from '../employeeContext';
import Axios from 'axios';

var fields= [];
export const FieldsModal = ({displayFieldsModal, setDisplayFieldsModal, options, setOptions}) => {
    //const [ options, setOptions ] = useState({employee_name: false, employee_age: false, employee_salary: false, active_status: false})
    const [ epmloyeeDetails, setEmployeeDetails ] = useContext(employeeContext)
    const customStyles = {
        content : {
          top                   : '50%',
          left                  : '50%',
          right                 : 'auto',
          bottom                : 'auto',
          marginRight           : '-50%',
          transform             : 'translate(-50%, -50%)',
          height                : '30%',
          width                 : '40%'
        }
    };

    const handleClick = (event) => {
        const value = Boolean(event.target.value);
        //console.log(value+" "+fieldsCheckbox)
        //console.log(options.employee_name+" name")
        //console.log(typeof event.target.name)

        if(options[event.target.name] === true){
            setOptions({
                ...options,
                [event.target.name] : false
            })
        }else{
            setOptions({
                ...options,
                [event.target.name] : true
            })
        }
        console.log(options)
    } 

    const fieldList = () => {
        /*for( var opt in options) {
            if(options[opt]){
                if(!(fields.includes(options[opt])))
                    fields.push(opt);
            }
        }
        console.log(fields)
        Axios.get('http://localhost:3001/employees', {
                params: {opt: [...new Set(fields)]
                }
        }).then((response) => {
            setEmployeeDetails(response.data);
            console.log('entered')
        })
        .catch(err => {
            alert(err);
        });*/
        setDisplayFieldsModal(false)
    }
    return  (
        <>
        <Modal 
            isOpen={displayFieldsModal} 
            onRequestClose={() => setDisplayFieldsModal(false)}
            style={customStyles}
        >
            <div className="fieldModal">
            
                    <h5>Select Fields:&nbsp;&nbsp;&nbsp;&nbsp;</h5><br></br>
                    <input id="checkbox" name="employee_salary"  checked={options.employee_salary} type="checkbox" value={!(options.employee_salary)} onClick={(e) => handleClick(e)}/>&nbsp;Salary<br></br>
                    <input id="checkbox" name="active_status"  checked={options.active_status} type="checkbox" value={!(options.active_status)} onClick={(e) => handleClick(e)}/>&nbsp;Active Status
                    <br></br><br></br>
                    <button onClick={() => fieldList()}>Done</button>
            </div>
        </Modal>
        
        </>
        
    )
}