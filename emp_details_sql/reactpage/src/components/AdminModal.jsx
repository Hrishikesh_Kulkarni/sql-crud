import React, {useState, useContext} from 'react';
import Modal from 'react-modal';
import Axios from 'axios';
import $ from 'jquery';
import { employeeContext } from '../employeeContext';


export const AdminModal = ({emp, adminModalOpen, setAdminModalOpen}) => {
    const customStyles = {
        content : {
          top                   : '50%',
          left                  : '50%',
          right                 : 'auto',
          bottom                : 'auto',
          marginRight           : '-50%',
          transform             : 'translate(-50%, -50%)',
          height                : '50%',
          width                 : '40%'
        }
    };
    const [ employeeDetails, setEmployeeDetails ] = useContext(employeeContext);
    const [updatedEmployee, setUpdatedEmployee] = useState({ newEmpId: '', newEmpName: '', newEmpAge: '', newEmpSalary: ''})
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [authenticated, setAuthenticated] = useState(false);
    const [adminInput, setAdminInput] = useState(true);

    const handleChange = (event) => {
        const value = event.target.value;
        setUpdatedEmployee({ 
            ...updatedEmployee,
            [event.target.name]: value
        })
    }

    const authenticateAdmin = () => {       
       if(username !== '' && password !== ''){
            //if(username.length === 4 && password.length === 4){
            Axios.get(`http://localhost:3001/api/auth/${username}&${password}`).then((response) => {
                console.log(response.data);
                if(response.data.length!=0){
                    setAuthenticated(true);
                    setAdminInput(false);
                }
            });
       }else{
           alert('Invalid Inputs');
       }
    }

    const updateEmp = (updateEmp_Id) => {
        if( updatedEmployee.newEmpId=== '' || updatedEmployee.newEmpName === '' || updatedEmployee.newEmpAge === '' || updatedEmployee.newEmpSalary === '' || updatedEmployee.newEmpPassword === ''){
            alert('Input Fields Empty!');
        }else{
            Axios.put("http://localhost:3001/employees", 
            {
                employee_id: updateEmp_Id,
                newEmployee_id: updatedEmployee.newEmpId,
                employee_name : updatedEmployee.newEmpName,
                employee_age: updatedEmployee.newEmpAge,
                employee_salary: updatedEmployee.newEmpSalary
            }).catch(err => alert(err));  

            setTimeout(() => {
                Axios.get("http://localhost:3001/employees").then((response) => {
                    console.log(response.data)
                    setEmployeeDetails(response.data);
                }).catch(err  => alert(err));
            }, 1000);
            setAdminModalOpen(false);
            setTimeout(() => {$('#empUpdate').fadeOut();}, 2000)
        }
    }

    return (
        <Modal 
            isOpen={adminModalOpen} 
            onRequestClose={() => setAdminModalOpen(false)}
            style={customStyles}
        >
            {adminInput && <><h2>Welcome Admin</h2>
            <label>Username</label>
            <input type="text" class="form-control" onChange={(e) => setUsername(e.target.value)}/>
            <label>Password</label>
            <input type="password" class="form-control" onChange={(e) => setPassword(e.target.value)} required/>
            <br></br>
            <button class="btn btn-success" onClick={() => authenticateAdmin()}>Submit</button></>}&nbsp;
            
            {authenticated && <><div className="updateForm">
                <label>Employee Id</label>
                <input type="text" class="form-control" name="newEmpId" value={updatedEmployee.newEmpId} onChange={handleChange}/>
                <label>Employee Name</label>
                <input type="text" class="form-control" name="newEmpName" value={updatedEmployee.newEmpName} onChange={handleChange}/>
                <label>Employee Age</label>
                <input type="text" class="form-control" name="newEmpAge" value={updatedEmployee.newEmpAge} onChange={handleChange}/>
                <label>Employee Salary</label>
                <input type="text" class="form-control" name="newEmpSalary" value={updatedEmployee.newEmpSalary} onChange={handleChange}/>
            </div>
            <br></br>
            <button class="btn btn-primary" onClick={() => updateEmp(emp.employee_id)}>Update</button>&nbsp;</>}
            <button class="btn btn-danger" onClick={() => setAdminModalOpen(false)}>Close</button>
        </Modal>
    )
}